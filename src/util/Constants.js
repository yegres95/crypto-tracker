export const apiBaseURL = 'https://api.binance.com/api/v1/klines';

/* TODO: LATER PULL DIRECTLY FROM BINANCE OR COINMARKET */
const btcData = {
  pairName: 'BTCUSDT',
  name: 'Bitcoin',
  symbol: 'BTC'
};
const ethData = {
  pairName: 'ETHUSDT',
  name: 'Ethereum',
  symbol: 'ETH'
};
const xrpData = {
  pairName: 'XRPUSDT',
  name: 'Ripple',
  symbol: 'XRP'
};
const ltcData = {
  pairName: 'LTCUSDT',
  name: 'Litcoin',
  symbol: 'LTC'
};
const eosData = {
  pairName: 'EOSUSDT',
  name: 'EOS.IO',
  symbol: 'EOS'
};
export const pairsToPull = [btcData, ethData, xrpData, ltcData, eosData]
