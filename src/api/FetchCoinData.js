import axios from 'axios';
import { apiBaseURL, pairsToPull } from './../util/Constants';
import {
    FETCHING_COIN_DATA,
    FETCHING_COIN_DATA_SUCCESS,
    FETCHING_COIN_DATA_FAIL,
} from './../util/ActionTypes';


function buildUrl(pair) {
  return `${apiBaseURL}?symbol=${pair}&interval=1h&limit=1000`
}

function buildUrls() {
  const urls = [];
  pairsToPull.forEach(function(pair) {
    urls.push(buildUrl(pair.pairName));
  });
  return urls;
}

function checkFailed (then) {
  return function (responses) {
    const someFailed = responses.some(response => response.error)
    if (someFailed) {
      throw responses
    }
    return then(responses)
  }
}

function getChange(startPrice, endPrice) {
  return Math.round(((endPrice-startPrice)/startPrice)*10000)/100;
}

function processData(res) {
  const data = {};
  res.forEach(function (res, index) {
    const priceData = res.data;
    const currentPrice = priceData[priceData.length-1][4];
    const price24hAgo = priceData[priceData.length-24][4];
    const price7dAgo = priceData[priceData.length-168][4];
    const price14dAgo = priceData[priceData.length-336][4];
    const price30dAgo = priceData[priceData.length-420][4];

    data[pairsToPull[index].symbol] = pairsToPull[index]; //base info
    data[pairsToPull[index].symbol].data = res.data; //all price info
    data[pairsToPull[index].symbol].price_usd = Math.round(currentPrice*100)/100; //get last data sets close price
    data[pairsToPull[index].symbol].percent_change_24h =  getChange(price24hAgo, currentPrice);
    data[pairsToPull[index].symbol].percent_change_7d = getChange(price7dAgo, currentPrice);
    data[pairsToPull[index].symbol].percent_change_14d = getChange(price14dAgo, currentPrice);
    data[pairsToPull[index].symbol].percent_change_30d = getChange(price30dAgo, currentPrice);
  })
  return data;
}

export default function FetchCoinData(dispatch) {
    dispatch({ type: FETCHING_COIN_DATA })
    return axios.all(buildUrls().map(url => axios.get(url)))
          .then(axios.spread(function (...res) {
              checkFailed(res)
              const data = processData(res)
              return dispatch({ type: FETCHING_COIN_DATA_SUCCESS, payload: data });
          })).catch((err) => {
            console.log(err)
            alert('FAILED TO LOAD', err)
          });
}
