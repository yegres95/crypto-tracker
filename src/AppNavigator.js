import { createStackNavigator, createAppContainer } from 'react-navigation';
import CryptoContainer from './components/CryptoContainer';
import CoinGraph from './components/CoinGraph';

const AppNavigator = createStackNavigator({
  Home: { screen: CryptoContainer },
  Graph: { screen: CoinGraph },
},
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const App = createAppContainer(AppNavigator);

export default App;
