import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { CryptoContainer } from './components/index';
import App from './AppNavigator';

const instructions = Platform.select({
  ios: 'ios',
  android: 'android',
});

export default class Index extends Component {
  render() {
    return (
      <App/>
    );
  }
}
