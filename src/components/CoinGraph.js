import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, ScrollView, StyleSheet, Button, Dimensions  } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from 'react-native-chart-kit'

class CoinGraph extends Component {

    componentWillMount() {
        //console.log(this.props.crypto.graphData)
    }

    parseData() {
      data = []
      const limit = 200; //performance issues with SVG generation
      var graphData = this.props.crypto.graphData.data;
      graphData = graphData.slice(graphData.length-limit, graphData.length-1);
       graphData.forEach(function (dataUnit, index) {
         data.push(dataUnit[4]); //Close price
       });
      return data;
    }

    render() {

        const { crypto } = this.props;
        const { contentContainer } = styles;

        if (crypto.isFetching) {
            return (
                <View>
                    <Spinner
                        visible={crypto.isFetching}
                        textContent={"Loading..."}
                        textStyle={{color: '#253145'}}
                        animation="fade"
                    />
                </View>
            )
        }

        return (
            <ScrollView contentContainerStyle={contentContainer}>
              <LineChart
                data={{
                  labels: [],
                  datasets: [{
                    data: this.parseData()
                  }]
                }}
                width={Dimensions.get('window').width}
                height={Dimensions.get('window').height-150}
                yAxisLabel={'$'}
                chartConfig={{
                  backgroundColor: 'rgba(0,0,255,0.2)',
                  backgroundGradientFrom: 'rgba(0,0,0,1)',
                  backgroundGradientTo: 'rgba(0,0,255,0.2)',
                  decimalPlaces: 2,
                  color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                  style: {
                    borderRadius: 6
                  }
                }}
                bezier
                style={{
                  marginVertical: 0,
                  borderRadius: 0
                }}
              />
              <Button title="GO BACK" onPress={() => this.props.navigation.navigate('Home') }/>
            </ScrollView>
        )


    }
}

const styles = {
    contentContainer: {
        paddingBottom: 100,
        paddingTop: 55
    }
}

function mapStateToProps(state) {
    return {
        crypto: state.crypto
    }
}

export default connect(mapStateToProps)(CoinGraph)
