import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, ScrollView, StyleSheet, Button, TouchableHighlight  } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import FetchCoinData from './../api/FetchCoinData';
import CoinCard from './CoinCard';

import { USE_COIN_DATA } from './../util/ActionTypes';

class CryptoContainer extends Component {

    componentWillMount() {
        this.props.FetchCoinData(this.props.dispatch);
    }

    renderGraph(coinSymbol) {
      this.props.dispatch({ type: USE_COIN_DATA, coinSymbol: coinSymbol });
      this.props.navigation.navigate('Graph')
    }

    renderCoinCards() {
        const { crypto } = this.props;
        const coinSymbols = Object.keys(crypto.data);
        return coinSymbols.map(coinSymbol =>
            <TouchableHighlight key={crypto.data[coinSymbol].name}
              onPress={() => this.renderGraph(coinSymbol)}>
              <CoinCard
                key={crypto.data[coinSymbol].name}
                coin_name={crypto.data[coinSymbol].name}
                symbol={crypto.data[coinSymbol].symbol}
                price_usd={crypto.data[coinSymbol].price_usd}
                percent_change_24h={crypto.data[coinSymbol].percent_change_24h}
                percent_change_7d={crypto.data[coinSymbol].percent_change_7d}
                percent_change_14d={crypto.data[coinSymbol].percent_change_14d}
                percent_change_30d={crypto.data[coinSymbol].percent_change_30d}
              />
            </TouchableHighlight >
        )
    }


    render() {

        const { crypto } = this.props;
        const { contentContainer } = styles;

        if (crypto.isFetching) {
            return (
                <View>
                    <Spinner
                        visible={crypto.isFetching}
                        textContent={"Loading..."}
                        textStyle={{color: '#253145'}}
                        animation="fade"
                    />
                </View>
            )
        }

        return (
            <ScrollView contentContainerStyle={contentContainer}>
                {this.renderCoinCards()}
            </ScrollView>
        )


    }
}

const styles = {
    contentContainer: {
        paddingBottom: 100,
        paddingTop: 55
    }
}

function mapStateToProps(state) {
    return {
        crypto: state.crypto,
        FetchCoinData: FetchCoinData
    }
}

export default connect(mapStateToProps)(CryptoContainer)
