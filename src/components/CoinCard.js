import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';
import { images } from '../util/CoinIcons';

const styles = StyleSheet.create({
    container: {
        display: "flex",
        marginBottom: 20,
        borderColor: "rgba(0,0,255,0.2)",
        borderWidth: 3,
        borderRadius: 5,
        padding: 20,
        margin: 20,
        backgroundColor: "rgba(255,255,255,1)",
        shadowColor: "#000",
        shadowOffset: {
        	width: 0,
        	height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10
    },
    upperRow: {
        display: "flex",
        flexDirection: "row",
        marginBottom: 15
    },
    coinSymbol: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 5,
        fontWeight: "bold",
    },
    coinName: {
        marginTop: 10,
        marginLeft: 5,
        marginRight: 20
    },
    seperator: {
        marginTop: 10,
    },
    coinPrice: {
        marginTop: 10,
        marginLeft: "auto",
        marginRight: 10,
        fontWeight: "bold",
    },
    image: {
        width: 35,
        height: 35,
    },
    moneySymbol: {
        fontWeight: "bold",
    },
    statisticsContainer: {
        display: "flex",
        borderTopColor: "rgba(0,0,255,0.1)",
        borderTopWidth: 2,
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-around"
    },
    statisticsBox: {
      flexDirection: "column"
    },
    singleStat: {
        textAlign: 'center'
    },
    percentChangePlus: {
        color: "#00BFA5",
        fontWeight: "bold",
        textAlign: 'center'
    },
    percentChangeMinus: {
        color: "#DD2C00",
        fontWeight: "bold",
        textAlign: 'center'
    }
})

const {
    container,
    image,
    moneySymbol,
    upperRow,
    coinSymbol,
    coinName,
    coinPrice,
    statisticsContainer,
    statisticsBox,
    singleStat,
    seperator,
    percentChangePlus,
    percentChangeMinus
} = styles;

const CoinCard = ({ symbol, coin_name, price_usd, percent_change_24h, percent_change_7d, percent_change_14d, percent_change_30d }) => {

    return (
        <View style={container}>

            <View style={upperRow}>
                <Image
                    style={styles.image}
                    source={{ uri: images[symbol] }}
                />
                <Text style={coinSymbol}>{symbol}</Text>
                <Text style={seperator}>|</Text>
                <Text style={coinName}>{coin_name}</Text>
                <Text style={coinPrice}>{price_usd}
                    <Text style={moneySymbol}> $ </Text>
                </Text>
            </View>

            <View style={statisticsContainer}>
                <View style={statisticsBox}>
                     <Text style={singleStat}>24h</Text>
                     <Text style={percent_change_24h < 0 ? percentChangeMinus : percentChangePlus }> {percent_change_24h} % </Text>
                </View>
                <View style={statisticsBox}>
                    <Text style={singleStat}>7d</Text>
                    <Text style={percent_change_7d < 0 ? percentChangeMinus : percentChangePlus }> {percent_change_7d} % </Text>
                </View>
                <View style={statisticsBox}>
                    <Text style={singleStat}>14d</Text>
                    <Text style={percent_change_14d < 0 ? percentChangeMinus : percentChangePlus }> {percent_change_14d} % </Text>
                </View>
                <View style={statisticsBox}>
                    <Text style={singleStat}>30d</Text>
                    <Text style={percent_change_30d < 0 ? percentChangeMinus : percentChangePlus }> {percent_change_30d} % </Text>
                </View>
            </View>

        </View>
    );
}



export default CoinCard;
