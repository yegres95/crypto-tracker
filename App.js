import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Index from './src/index';
import Store from './src/Store';

export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <Index />
      </Provider>
    );
  }
}
