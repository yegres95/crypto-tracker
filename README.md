## Install

1. Clone repo.
2. **npm install**.

---

## Running

**npm run android**

**npm run ios** - Not tested as did not have a MAC on hand to install cocoa pods or to run in iOS emulator


## Extras
Extremely basic test implemented. Would need a lot of testing in the API and reducers, especially if more functionality was added.     
Basic graphs added but would require a custom graph component to draw the data points without SVG (or another library) to reduce lag with larger data sets.      
Multiple buttons can be added under the graph to select different time scales, the API would not require much tweaking.
